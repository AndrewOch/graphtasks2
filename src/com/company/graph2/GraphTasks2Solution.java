package com.company.graph2;

import java.util.HashMap;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {

        Graph graph = new Graph();
        return graph.goDijkstra(adjacencyMatrix, startIndex);
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        Graph graph = new Graph();
        return graph.goPrima(adjacencyMatrix);
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        Graph graph = new Graph();
        return graph.goKruskal(adjacencyMatrix);
    }
}
